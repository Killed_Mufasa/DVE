﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Xml.Linq;

namespace MainDataHandler
{
    public class XmlConverter
    {
        // Make a controller
        private readonly DBControllers.DynamicTableController _dbTableController;

        // Make a list of strings for the value names
        private readonly List<string> _xmlValueNames;
        private readonly List<XmlColumns> _xmlColumns;
        private readonly string _tableName;
        private int _elementCounter;
        private readonly int _rowsPerTransaction;

        // The filepath for the xml file
        private readonly string _xmlFilePath;

        /// <summary>
        /// Constructor for the XMl converter class
        /// </summary>
        /// <param name="xmlFilePath">The filepath of the xml file</param>
        /// <param name="connectString">Settings used to connect to the database
        ///     SQLite format:
        ///         "Data Source=filePath; Password=password"
        ///     MySQL format:
        ///         "server=serveradress; user=user; password=password; database=database;"
        /// </param>
        /// <param name="databaseType"></param>
        /// <param name="tableName">The tablename for the data stored in the sql system.</param>
        public XmlConverter(string xmlFilePath, DbConnection dbConnection, string tableName)
        {
            this._dbTableController = new DBControllers.DynamicTableController(dbConnection);
            this._xmlFilePath = xmlFilePath;
            this._xmlColumns = new List<XmlColumns>();
            this._xmlValueNames = new List<string>();
            this._tableName = tableName;
            this._elementCounter = 0;
            this._rowsPerTransaction = 5000;
        }


        /// <summary>
        /// Returns an empty querystring
        /// </summary>
        /// <returns>Empty querystring</returns>
        private string EmptyInsertQuery()
        {
            StringBuilder queryBuilder = new StringBuilder();

            queryBuilder.Append($"INSERT INTO {this._tableName} (");
            int i = 0;

            // loop trough all xml columns
            foreach (string xmlValueName in _xmlValueNames)
            {
                if (i == _xmlValueNames.Count - 1)
                {
                    queryBuilder.Append($"{xmlValueName}");
                }
                else
                {
                    queryBuilder.Append($"{xmlValueName}, ");

                }
                i++;
            }
            queryBuilder.Append(") VALUES ");

            return queryBuilder.ToString();
        }

        /// <summary>
        /// Create a new Database file
        /// </summary>
        public void CreateDatabase()
        {
            // Create new xmlReader
            DynamicXmlReader reader = new DynamicXmlReader(this._xmlFilePath);

            this._xmlColumns.Add(new XmlColumns { Id = _elementCounter, Name = "RowId", Pk = true, Alias = "NotFilledIn" });

            this._xmlValueNames.Add("RowId");

#if DEBUG
            Console.WriteLine("Added column: RowId");
            Console.WriteLine($"Added column with the following information: {_xmlColumns[_elementCounter].Id} {_xmlColumns[_elementCounter].Name}");
#endif
            //increases everytime, if there is a unique element
            this._elementCounter++;

            // Loop through all XML elements, increment for every new element
            foreach (XElement element in reader.XElements())
            {
                // If the XML elements name is new, add it to the list of names
                if (!this._xmlValueNames.Contains(element.Name.ToString()))
                {
                    this._xmlColumns.Add(new XmlColumns { Id = _elementCounter, Name = element.Name.ToString(), Pk = false, Alias = "NotFilledIn" });

                    this._xmlValueNames.Add(element.Name.ToString());

#if DEBUG
                    Console.WriteLine("Added column:" + element.Name);
                    Console.WriteLine($"Added column with the following information: {_xmlColumns[_elementCounter].Id} {_xmlColumns[_elementCounter].Name}");
#endif
                    //increases everytime, if there is a unique element
                    this._elementCounter++;
                }
                else
                {
                    Console.WriteLine(_elementCounter);
                    // make database with element names
                    // Create a database with the xmlValueNames
                    this._dbTableController.AddColumns(this._xmlFilePath, this._xmlValueNames, this._tableName);
                    break;
                }
            }

#if DEBUG
            Console.WriteLine("{0} Elements", this._nrOfColumns);
#endif
        }

        public void CreateCleanDatabase(string creationStatement)
        {
            this._dbTableController.ExecuteStatement(creationStatement);
        }


        private int CountColumns()
        {
            int retVal = 0;
            DynamicXmlReader reader = new DynamicXmlReader(this._xmlFilePath);

            foreach (XElement element in reader.XElements())
            {
                // If the XML elements name is new, add it to the list of names
                if (!this._xmlValueNames.Contains(element.Name.ToString()))
                {
                    this._xmlColumns.Add(new XmlColumns { Id = _elementCounter, Name = element.Name.ToString(), Pk = false, Alias = "NotFilledIn" });

                    this._xmlValueNames.Add(element.Name.ToString());
                    //increases everytime, if there is a unique element
                    retVal++;
                }
                else
                    break;
                }
            return retVal;
        }

        /// <summary>
        /// Write data to the database
        /// </summary>
        public void WriteXmlToDatabase()
        {
            StringBuilder queryBuilder = new StringBuilder();
            int nrOfColums = this.CountColumns();

            // Create new xml reader
            DynamicXmlReader reader = new DynamicXmlReader(this._xmlFilePath);
            int elementCounter = 0; // counts amount of elements handled
            int rowCounter = 0; // counts amount of rows handled

            queryBuilder.Append(EmptyInsertQuery());

            // Loop through all XML elements and add 1 to i for every new element
            foreach (XElement element in reader.XElements())
            {
                elementCounter++;

                if (elementCounter == 1) // If it's the first element in the row, add '('
                {
                    if (rowCounter != 0)
                    {
                        queryBuilder.Append(", ");
                    }
                    queryBuilder.Append("(null");
                    elementCounter++;
#if DEBUG
                    Console.WriteLine($"De elementcounter is nu {elementCounter}");
#endif
                }

                if (elementCounter != nrOfColums) // If it's neither the first, nor the last element in the row
                {
                    queryBuilder.Append($", '{ element.Value}'");
                }
                else // If it's the last element in the row, add ')' 
                {
                    queryBuilder.Append($", '{ element.Value}')");
                    rowCounter++; // add 1 to rows done
                    elementCounter = 0;
#if DEBUG
                    Console.WriteLine($"rowCounter is omhoog en is nu {rowCounter}");
#endif
                }

                // If the amount of rows per package has been reached, cal PutRows -> write to database
                if (rowCounter >= this._rowsPerTransaction)
                {
#if DEBUG
                    Console.WriteLine("INFO: transaction is ready to be inserted");
#endif

                    queryBuilder.Append(";"); // add ; to finish transaction-string

                    // make connection and write to database
                    this._dbTableController.PutRows(queryBuilder);

                    // Clean up the StringBuilder (faster than emptying out current one)
                    queryBuilder = new StringBuilder();
                    queryBuilder.Append(EmptyInsertQuery());
                    rowCounter = 0; // reset rows-counter
                }
            }

            if(queryBuilder.Length > 0)
            {
#if DEBUG
                Console.WriteLine("INFO: Transaction could not be filled, but is ready to be inserted");
                Console.WriteLine($"DEBUG: rowsPerTransaction is {this._rowsPerTransaction}, rowCounter is {rowCounter}");
#endif
                queryBuilder.Append(";"); // add ; to finish package-string
                                          //Console.WriteLine(queryBuilder);

                // make connection and write to database
                this._dbTableController.PutRows(queryBuilder);
            }
        }

        public void WriteXMLToCleanDatabase(List<string> notNullList)
        {
            StringBuilder queryBuilder = new StringBuilder();
            List<string> rowString = new List<string>();

            int nrOfTransactions = 0;

            bool skipRow = false;

            int nrOfColums = this.CountColumns();

            // Create new xml reader
            DynamicXmlReader reader = new DynamicXmlReader(this._xmlFilePath);
            int elementCounter = 0; // counts amount of elements handled
            int rowCounter = 0; // counts amount of rows handled

            queryBuilder.Append(EmptyInsertQuery());

            //reader.ValidateXmlFile();

            // Loop through all XML elements and add 1 to i for every new element
            foreach (XElement element in reader.XElements())
            {
                elementCounter++;

                if (!skipRow)
                {
                    switch (element.Name.ToString())
                    {
                        case "Trip_ID":
                        case "Taxi_ID":
                            if (element.Value.ToString().Equals(string.Empty))
                            {
                                skipRow = true;
                            }
                            else
                            {
                                rowString.Add($"'{element.Value.ToString()}'");
                            }
                            break;
                        case "Trip_Start_Timestamp":
                        case "Trip_End_Timestamp":
                            if (element.Value.ToString().Equals(string.Empty))
                            {
                                rowString.Add("null");
                            }
                            else
                            {
                                DateTime dateTime = DateTime.ParseExact(element.Value.ToString(), "MM/dd/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
                                rowString.Add($"'{dateTime.ToString("s", CultureInfo.CreateSpecificCulture("en-US"))}'");
                            }
                            break;
                        case "Trip_Seconds":
                        case "Pickup_Community_Area":
                        case "Dropoff_Community_Area":
                        case "Pickup_Census_Tract":
                        case "Dropoff_Census_Tract":
                            Int64 _outInt;
                            if (Int64.TryParse(element.Value, out _outInt))
                            {
                                rowString.Add(_outInt.ToString());
                            }
                            else
                            {
                                rowString.Add("null");
                            }
                            break;
                        case "Trip_Miles":
                        case "Fare":
                        case "Tips":
                        case "Tolls":
                        case "Extras":
                        case "Trip_Total":
                            double _outDouble;
                            if (double.TryParse(element.Value, NumberStyles.Number, CultureInfo.CreateSpecificCulture("en-US"), out _outDouble))
                            {
                                rowString.Add(element.Value.ToString());
                            }
                            else
                            {
                                rowString.Add("null");
                            }
                            break;
                        case "Payment_Type":
                            List<string> paymentTypes = new List<string> { "Credit Card", "Cash", "Mobile", "Prcard", "Unknown", "No Charge", "Dispute", "Prepaid" };
                            if (paymentTypes.Contains(element.Value.ToString()))
                            {
                                rowString.Add($"'{element.Value.ToString()}'");
                            }
                            else
                            {
                                skipRow = true;
                            }
                            break;
                        case "Company":
                            if (element.Value.ToString().Equals(string.Empty))
                            {
                                rowString.Add("null");
                            }
                            else
                            {
                                rowString.Add($"\"{element.Value.ToString()}\"");
                            }
                            break;
                        case "Pickup_Centroid_Latitude":
                        case "Pickup_Centroid_Longitude":
                        case "Dropoff_Centroid_Latitude":
                        case "Dropoff_Centroid_Longitude":
                            //parse double not null
                            if (element.Value.ToString().Equals(string.Empty))
                            {
                                skipRow = true;
                            }
                            else
                            {
                                double _outPosition;
                                if (double.TryParse(element.Value, out _outPosition))
                                {
                                    rowString.Add(element.Value.ToString());
                                }
                                else
                                {
                                    skipRow = true;
                                }
                            }
                            break;
                        case "Pickup_Centroid_Location":
                        case "Dropoff_Centroid__Location":
                            //string not null
                            if (element.Value.ToString().Equals(string.Empty))
                            {
                                skipRow = true;
                            }
                            else
                            {
                                rowString.Add($"'{element.Value.ToString()}'");
                            }
                            break;
                    }

                }

                if (elementCounter == nrOfColums) // If it's neither the first, nor the last element in the row
                {
                    if (!skipRow)
                    {
                        if (rowCounter != 0)
                        {
                            queryBuilder.Append(", ");
                        }

                        queryBuilder.Append("(");

                        
                        foreach (string row in rowString)
                        {
                            if (rowString[0].Equals(row))
                            {
                                queryBuilder.Append($"{row}");
                            }
                            else
                            {
                                queryBuilder.Append($", {row}");
                            }
                            
                        }
                        queryBuilder.Append(")");
                        rowCounter++; // add 1 to rows done
#if DEBUG
                        Console.WriteLine($"rowCounter is omhoog en is nu {rowCounter}");
#endif
                    }

                    rowString.Clear();
                    skipRow = false;
                    elementCounter = 0;
                }

                // If the amount of rows per package has been reached, cal PutRows -> write to database
                if (rowCounter >= this._rowsPerTransaction)
                {
#if DEBUG
                    Console.WriteLine("INFO: transaction is ready to be inserted");
#endif

                    queryBuilder.Append(";"); // add ; to finish transaction-string

                    // make connection and write to database
                    this._dbTableController.PutRows(queryBuilder);

                    // Clean up the StringBuilder (faster than emptying out current one)
                    queryBuilder = new StringBuilder();
                    queryBuilder.Append(EmptyInsertQuery());
                    rowCounter = 0; // reset rows-counter
                    nrOfTransactions++;
                    Console.WriteLine($"INFO: Inserted batch number {nrOfTransactions}, with a batchSize of {this._rowsPerTransaction}");
                }
            }

            if (queryBuilder.Length > 0)
            {
#if DEBUG
                Console.WriteLine("INFO: Transaction could not be filled, but is ready to be inserted");
                Console.WriteLine($"DEBUG: rowsPerTransaction is {this._rowsPerTransaction}, rowCounter is {rowCounter}");
#endif
                queryBuilder.Append(";"); // add ; to finish package-string
                                          //Console.WriteLine(queryBuilder);

                // make connection and write to database
                this._dbTableController.PutRows(queryBuilder);
            }
        }
    }
}
