﻿namespace MainDataHandler.DBControllers
{
    public class ColumnNames
    {
        public int ColumnID { get; set; }
        public string ColumnName { get; set; }
    }
}
