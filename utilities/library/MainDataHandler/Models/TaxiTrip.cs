﻿namespace MainDataHandler.Models
{
    public class TaxiTrip
    {
        public int RowId { get; internal set; }
        public string TripId { get; internal set; }
        public string TaxiId { get; internal set; }
        public float TripTotal { get; internal set; }
        public float PickupCentroidLatitude { get; internal set; }
        public float PickupCentroidLongitude { get; internal set; }
        public float DropoffCentroidLatitude { get; internal set; }
        public float DropoffCentroidLongitude { get; internal set; }

        public TaxiTrip(int rowId)
        {
            this.RowId = rowId;
        }
    }
}
