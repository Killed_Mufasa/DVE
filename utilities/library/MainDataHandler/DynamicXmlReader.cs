﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

namespace MainDataHandler
{
    /// <summary>
    /// XML reader, which is internally reading the file line by line
    /// </summary>
    internal class DynamicXmlReader
    {
        private readonly string _filePath;
        private int _exceptionLineNumber;

        public DynamicXmlReader(string filePath)
        {
            this._filePath = filePath;
            this._exceptionLineNumber = 0;
        }

        public void ValidateXmlFile()
        {
            int currentLine = 0;
            List<int> rottenApples = new List<int>();

            XmlReader reader = XmlReader.Create(this._filePath);
            while (!reader.EOF)
            {
                try
                {
                    IXmlLineInfo xmlInfo = reader as IXmlLineInfo;
                    reader.MoveToContent();
                    if (rottenApples.Count > 0)
                    {
                        for (int i = 0; i < (rottenApples[rottenApples.Count -1]); i++)
                        {
                            reader.MoveToNextAttribute();
                            reader.Skip();
                        }
                        reader.MoveToAttribute("row");
                    }

                    while(reader.Read())
                    {
                        currentLine = xmlInfo.LineNumber;
                    }
                        
                }
                catch (XmlException ex)
                {
                    rottenApples.Add(ex.LineNumber);
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        public IEnumerable<XElement> XElements()
        {
            using (XmlReader reader = XmlReader.Create(this._filePath))
            {
                IXmlLineInfo xmlInfo = reader as IXmlLineInfo;

                reader.MoveToContent();
                // Parse the file and return each of the child_node

                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element && reader.Name != "row")
                    {
                        XElement el = null;

                        try
                        {
                            el = XNode.ReadFrom(reader) as XElement;
                        }
                        catch (XmlException ex)
                        {
                            System.Console.WriteLine("Row geskipped vanwege xml parse error" + ex.Message);
                            reader.Skip();
                            reader.ReadToNextSibling("row");
                            this._exceptionLineNumber = ex.LineNumber;
                        }

                        if ( el != null )
                        {
                            yield return el;
                        }
                    }
                }
            }
        }
    }
}
