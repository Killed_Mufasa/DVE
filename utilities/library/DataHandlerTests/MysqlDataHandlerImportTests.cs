﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using MySql.Data.MySqlClient;
using System.Data.Common;

namespace MainDataHandler.Tests
{
    [TestClass()]
    public class MysqlDataHandlerImportTests
    {
        private XmlConverter _dataHandler;
        private DbConnection _databaseConnection;
        private readonly string _tableName = "testdata";

        [TestInitialize()]
        public void Init()
        {
            string connectString = File.ReadAllText("C:\\CI\\DVE\\dbtests.env");
            this._databaseConnection = new MySqlConnection(connectString);
            this._databaseConnection.Open();
            DbCommand command = this._databaseConnection.CreateCommand();
            command.CommandText = $"drop table if exists {this._tableName}";
            command.ExecuteNonQuery();
            this._databaseConnection.Close();

            this._dataHandler = new XmlConverter("C:\\CI\\DVE\\taxi-tests.xml", this._databaseConnection, this._tableName);
            this._dataHandler.CreateDatabase();
        }

        [TestMethod()]
        public void CreateDatabaseTest()
        {
            int columnCount = 0;
            int expect = 24;
            try
            {
                this._databaseConnection.Open();
                DbCommand command = this._databaseConnection.CreateCommand();
                command.CommandText = $"SHOW COLUMNS FROM dve_dev.{this._tableName}";
                DbDataReader dbReader = command.ExecuteReader();
                while (dbReader.Read())
                {
                    columnCount++;
                }
            }

            finally
            {
                this._databaseConnection.Close();
            }
    
            Assert.AreEqual(expect, columnCount);
        }

        [TestMethod()]
        public void WriteXmlToDatabaseTest()
        {
            int assertValue = 0;
            this._dataHandler.WriteXmlToDatabase();
            try
            {
                this._databaseConnection.Open();
                DbCommand command = this._databaseConnection.CreateCommand();
                command.CommandText = $"select count(*) from {this._tableName};";
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    assertValue = reader.GetInt32(0);
                }
            }

            finally
            {
                this._databaseConnection.Close();
            }

            Assert.AreEqual(100009, assertValue);
        }
    }
}
