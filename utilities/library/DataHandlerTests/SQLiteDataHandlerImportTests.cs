﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Data.SQLite;
using System.Data.Common;

namespace MainDataHandler.Tests
{
    [TestClass()]
    public class SQLiteDataHandlerImportTests
    {
        private XmlConverter _dataHandler;
        private DbConnection _databaseConnection;
        private readonly string _connectString = "Data Source=test.sqlite3;Password=Test123";
        private readonly string _tableName = "testdata";

        [TestInitialize()]
        public void Init()
        {
            if(File.Exists("test.sqlite3"))
            {
                File.Delete("test.sqlite3");
            }

            this._databaseConnection = new SQLiteConnection(this._connectString);
            this._dataHandler = new XmlConverter("C:\\CI\\DVE\\taxi-tests.xml", this._databaseConnection, this._tableName);
            this._dataHandler.CreateDatabase();
#pragma warning disable S1215 // "GC.Collect" should not be called
            GC.Collect();
#pragma warning restore S1215 // "GC.Collect" should not be called
            GC.WaitForPendingFinalizers();
        }

        [TestMethod()]
        public void CreateDatabaseTest()
        {
            Assert.IsTrue(File.Exists("test.sqlite3"));
        }

        [TestMethod()]
        public void WriteXmlToDatabaseTest()
        {
            int assertValue = 0;
            this._dataHandler.WriteXmlToDatabase();
            try
            {
                this._databaseConnection.Open();
                DbCommand Command = this._databaseConnection.CreateCommand();
                Command.CommandText = $"select count(*) from {this._tableName};";
                DbDataReader reader = Command.ExecuteReader();
                while (reader.Read())
                {
                    assertValue = reader.GetInt32(0);
                }
            }

            finally
            {
                this._databaseConnection.Close();
            }

            Assert.AreEqual(100009, assertValue);
        }
    }
}
