﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Collections.Generic;
using System.Data.Common;
using MySql.Data.MySqlClient;

namespace MainDataHandler.Tests
{

    [TestClass()]
    public class MysqlMainDataHandlerTests
    {
        private DataHandler _dataHandler;
        private DbConnection _dbConnection;
        private readonly string _tableName = "testdata";

        [TestInitialize()]
        public void Init()
        {
            string connectString = File.ReadAllText("C:\\CI\\DVE\\dbtests.env");

            this._dbConnection = new MySqlConnection(connectString);
            try
            {
                this._dbConnection.Open();
                DbCommand command = this._dbConnection.CreateCommand();
                command.CommandText = $"drop table if exists {this._tableName}";
                command.ExecuteNonQuery();
            }

            finally
            {
                this._dbConnection.Close();
            }

            XmlConverter xmlConverter = new XmlConverter("C:\\CI\\DVE\\taxi-tests.xml", this._dbConnection, this._tableName);
            xmlConverter.CreateDatabase();
            xmlConverter.WriteXmlToDatabase();

            this._dataHandler = new DataHandler(this._dbConnection);
        }

        [TestMethod()]
        public void GetDataTest()
        {
            this._dataHandler = new DataHandler(this._dbConnection);
            List<Models.TaxiTrip> data = this._dataHandler.GetData(42f, -86f, 43f, -89f, this._tableName);
            Assert.IsTrue(data.Count > 20);
        }
    }
}
