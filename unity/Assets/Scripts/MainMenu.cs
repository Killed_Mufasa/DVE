﻿using UnityEngine;

public class MainMenu : MonoBehaviour
{
    /// <summary>
    /// When this method is called, the application will be closed.
    /// </summary>
    public void QuitMenu()
    {
        Debug.Log("Quiting");
        Application.Quit();
    }
}
