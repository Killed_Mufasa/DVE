## This project has been moved to: https://gitlab.com/d_v_e/DVE

> <b>Data Visualisation Environment (DVE)</b>
> 
> <b>Visualise data from a database in either Minecraft or Unity.</b>
> 
> Includes a CSV -> XML and XML -> SQLite or MySQL database converter.
> 
> If you want to run the Minecraft visualisation, make sure to:
> 1. Download Forge
> 2. Fill in all required fields (such as database specs)
> 3. Run the project by using the runClient
> 4. Start the visualisations-GUI by using the /dve menu command.
> 
> You can also use the GUI for experimental features by typing /dve more
> and /dve load can be used for a quick falling block spawner without using a GUI.
> 
> #### Credits
> This project was worked on by a group of Innovative Development students
> of The Hague University of Applied Sciences: @Killed_Mufasa, @joostvanviegen,
> @HHS-Stan, @pokemonspeler and @Exkellever. 
> 
> We open-sourced this project in the hope that it can contribute to innovative
> solutions for data visualisation around the world.
>  
> This project is licensed under the GNU General Public License v3.0.
