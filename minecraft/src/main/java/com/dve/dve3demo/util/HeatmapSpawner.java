package com.dve.dve3demo.util;

import com.dve.dve3demo.database.DatabaseHandler;
import com.dve.dve3demo.gui.SpawnerScreen;
import com.dve.dve3demo.network.BlockSpawner;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.util.text.StringTextComponent;
import org.jline.utils.Log;

import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class HeatmapSpawner {
    public static void loadHeatmap(Minecraft minecraft, BlockState blockStateStart, BlockState blockStateEnd, int batchSize, int batchesLeft, boolean isRandomized, boolean isServerside) {
        Set<Map<String, Integer>> data = DatabaseHandler.getBlockPosFromDatabase(batchSize*batchesLeft);
        SpawnerScreen.blocksUsedForStart.add(Objects.requireNonNull(blockStateStart.getBlock().getRegistryName()).getPath());
        SpawnerScreen.blocksUsedForEnd.add(Objects.requireNonNull(blockStateEnd.getBlock().getRegistryName()).getPath());

        BlockState outlierBlockStateStart = Blocks.JACK_O_LANTERN.getDefaultState();
        BlockState outlierBlockStateEnd = Blocks.GLOWSTONE.getDefaultState();
        SpawnerScreen.blocksUsedForStart.add(Objects.requireNonNull(outlierBlockStateStart.getBlock().getRegistryName()).getPath());
        SpawnerScreen.blocksUsedForEnd.add(Objects.requireNonNull(outlierBlockStateEnd.getBlock().getRegistryName()).getPath());

        int counter = 0;
        assert data != null;

        // If it is local and not randomized
        // Loads faster, but interaction is not possible
        if (!isServerside && !isRandomized) {
            for (Map<String, Integer> row : data) {
                counter++;
                if (counter % 100 == 0) {
                    Log.info("DVEDEBUG: Visualised " + counter + " rows in total");
                }
                if (row.get("outlier") == 1) {
                    BlockSpawner.localBlockSpawner(minecraft, outlierBlockStateStart, row.get("start_x"), row.get("start_z"));
                    BlockSpawner.localBlockSpawner(minecraft, outlierBlockStateEnd, row.get("end_x"), row.get("end_z"));
                } else {
                    BlockSpawner.localBlockSpawner(minecraft, blockStateStart, row.get("start_x"), row.get("start_z"));
                    BlockSpawner.localBlockSpawner(minecraft, blockStateEnd, row.get("end_x"), row.get("end_z"));
                }
            }
        }

        // If it is local but randomized
        // Loads faster, but interaction is not possible
        if (!isServerside && isRandomized) {
            for (Map<String, Integer> row : data) {
                counter++;
                if (counter % 100 == 0) {
                    Log.info("DVEDEBUG: Visualised " + counter + " rows in total");
                }

                BlockState randomizedBlock = BlockRandomizer.getRandomBlock();
                BlockSpawner.localBlockSpawner(minecraft, randomizedBlock, row.get("start_x"), row.get("start_z"));
                BlockSpawner.localBlockSpawner(minecraft, randomizedBlock, row.get("end_x"), row.get("end_z"));
            }
        }

        // If it is serversided but not randomized
        // Loads a bit slower, but interaction is possible
        if (isServerside && !isRandomized) {
            for (Map<String, Integer> row : data) {
                counter++;
                if (counter % SpawnerScreen.newHeightFrequency == 0) {
                    SpawnerScreen.ySpawnHeight += SpawnerScreen.changeHeightBy;
                    if (SpawnerScreen.ySpawnHeight > 255){
                        Log.warn("You might want to try with a smaller batchsize, a lower dY amount or a lower dY frequency to prevent breaking blocks.");
                        break;
                    }
                }

                if (row.get("outlier") == 1) {
                    BlockSpawner.remoteFallingBlockSpawner(minecraft, outlierBlockStateStart, row.get("start_x"), row.get("start_z"));
                    BlockSpawner.remoteFallingBlockSpawner(minecraft, outlierBlockStateEnd, row.get("end_x"), row.get("end_z"));
                } else {
                    BlockSpawner.remoteFallingBlockSpawner(minecraft, blockStateStart, row.get("start_x"), row.get("start_z"));
                    BlockSpawner.remoteFallingBlockSpawner(minecraft, blockStateEnd, row.get("end_x"), row.get("end_z"));
                }
            }
        }

        minecraft.player.sendMessage(new StringTextComponent("Visualised " + counter + " rows of data. 'A' = " + blockStateStart.getBlock().getRegistryName().getPath() + ". 'B' = " + blockStateEnd.getBlock().getRegistryName().getPath() + ". The limit was " + batchSize * batchesLeft + " rows."));
        minecraft.displayGuiScreen(null);
    }
}