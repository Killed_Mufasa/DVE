package com.dve.dve3demo;

import com.dve.dve3demo.blocks.*;
import com.dve.dve3demo.items.DataInspectorItem;
import com.dve.dve3demo.setup.ModSetup;
import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.loading.FMLPaths;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

// Main

// The value here should match an entry in the META-INF/mods.toml file
@Mod("dve3demo")
public class DVE {

    public static final String MODID = "dve3demo";

    public static final ModSetup setup = new ModSetup();

    public static final Logger LOGGER = LogManager.getLogger();

    public DVE() {
        ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, Config.CLIENT_CONFIG);
        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, Config.COMMON_CONFIG);

        // Register the setup method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);

        Config.loadConfig(Config.CLIENT_CONFIG, FMLPaths.CONFIGDIR.get().resolve("dve3demo-client.toml"));
        Config.loadConfig(Config.COMMON_CONFIG, FMLPaths.CONFIGDIR.get().resolve("dve3demo-common.toml"));
    }

    private void setup(final FMLCommonSetupEvent event) {
        setup.init();
    }

    // You can use EventBusSubscriber to automatically subscribe events on the contained class (this is subscribing to the MOD
    // Event bus for receiving Registry Events)
    @Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
    public static class RegistryEvents {

        @SubscribeEvent
        public static void onBlocksRegistry(final RegistryEvent.Register<Block> event) {
            event.getRegistry().register(new DveBlock());
            event.getRegistry().register(new TaxiAffiliationServicesBlock());
            event.getRegistry().register(new BlueRibbonBlock());
            event.getRegistry().register(new CityServiceBlock());
            event.getRegistry().register(new FlashCabBlock());
        }

        @SubscribeEvent
        public static void onItemsRegistry(final RegistryEvent.Register<Item> event) {
            Item.Properties properties = new Item.Properties()
                    .group(setup.itemGroup);
            event.getRegistry().register(new BlockItem(ModBlocks.DVEBLOCK, properties).setRegistryName("dveblock"));
            event.getRegistry().register(new BlockItem(ModBlocks.TAXIAFFILIATIONSERVICESBLOCK, properties).setRegistryName("taxiaffiliationservicesblock"));
            event.getRegistry().register(new BlockItem(ModBlocks.BLUERIBBONBLOCK, properties).setRegistryName("blueribbonblock"));
            event.getRegistry().register(new BlockItem(ModBlocks.CITYSERVICEBLOCK, properties).setRegistryName("cityserviceblock"));
            event.getRegistry().register(new BlockItem(ModBlocks.FLASHCABBLOCK, properties).setRegistryName("flashcabblock"));
            event.getRegistry().register(new DataInspectorItem());
        }
    }
}
