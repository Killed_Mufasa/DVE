package com.dve.dve3demo.database;

import com.dve.dve3demo.Config;
import com.dve.dve3demo.gui.SpawnerScreen;
import net.minecraft.client.Minecraft;
import net.minecraft.util.text.StringTextComponent;
import org.jline.utils.Log;

import java.sql.*;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class DatabaseHandler {

    public static final String connector = "mysql";
//    public static String connector = "sqlite";

    public DatabaseHandler() {

    }

    public static Connection getConnection() throws SQLException, ClassNotFoundException {
        String Connector = "mysql";
        //String Connector = "sqlite";

        Connection conn = null;

        switch (connector) {
            case "mysql":
            case "mariadb":
  
                String host = "ip-address";
                String port = "port";
                String database = "database-name";
                String user = "database-user";
                String password = "password";

                Class.forName("com.mysql.cj.jdbc.Driver");
                String url = "jdbc:mysql://" + host + ":" + port + "/" + database + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
                conn = DriverManager.getConnection(url, user, password);

                break;
            case "sqlite":
            case "sqlite3":

                Class.forName("org.sqlite.JDBC");
                String filepath = Config.FILE_PATH.get();
                conn = DriverManager.getConnection("jdbc:sqlite:" + filepath);

                break;
            default:
                throw new ClassNotFoundException("driver not available");
        }
        return conn;
    }


    public static Set<Map<String, Integer>> getBlockPosFromDatabase(int limit) {
        Connection connection;
        ResultSet resultSet = null;
        PreparedStatement statement = null;
        Set<Map<String, Integer>> results = new LinkedHashSet<>();
        try {
            connection = getConnection();
        } catch (SQLException e) {
            Log.error(e.getMessage());
            Log.error("MYSQL_error");
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException e) {
            Log.error("Class for the given connection is not available, please use another one");
            return null;
        }


        try {
            statement = connection.prepareStatement("SELECT d.id, d.start_x, d.start_z, d.end_x, d.end_z, if(high_tips.id is not null, true, false) AS 'outlier' FROM high_tips RIGHT JOIN (SELECT * FROM minecraft LIMIT ? OFFSET ?) d ON high_tips.id = d.id;");
            statement.setInt(1, limit);
            SpawnerScreen.rowsVisualised += limit;
            statement.setInt(2, SpawnerScreen.rowsVisualised);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Map<String, Integer> data = new HashMap<>();
                for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++)
                    data.put(resultSet.getMetaData().getColumnName(i), resultSet.getInt(i));

                results.add(data);
            }
            resultSet.close();

        } catch (SQLException e) {
            Log.error("An error occurred in the database handler");
            Minecraft.getInstance().player.sendMessage(new StringTextComponent("Error: SQLException"));
            e.printStackTrace();
        } finally {

            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return results;
    }

    public static Map<String, Integer> getDataFromView(int rowBatchSize, int x, int y, int z, boolean isStart) {
        Connection connection;
        ResultSet resultSet = null;
        PreparedStatement statement = null;
        Map<String, Integer> results = new HashMap<>();

        try {
            connection = getConnection();
        } catch (SQLException e) {
            Log.error(e.getMessage());
            Log.error("MYSQL_error");
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException e) {
            Log.error("Class for the given connection is not available, please use another one");
            return null;
        }

        try {

            Log.info("DVE: x=" + x + " y=" + y + " z=" + z + " l=" + (y - SpawnerScreen.yStartHeight));
            if (isStart){
                statement = connection.prepareStatement("SELECT id, end_x, end_z " +
                        "FROM minecraft WHERE start_x = ? and start_z = ? and end_x is not null" +
                        " and end_z is not null LIMIT ?,1");
            }
            else {
                statement = connection.prepareStatement("SELECT id, start_x, start_z " +
                        "FROM minecraft WHERE end_x = ? and end_z = ? and start_x is not null" +
                        " and start_z is not null LIMIT ?,1");
            }
            statement.setInt(1, x);
            statement.setInt(2, z);
            statement.setInt(3, y - SpawnerScreen.yStartHeight);
            Log.info(statement);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                    results.put(resultSet.getMetaData().getColumnName(i), resultSet.getInt(i));
                }
            }
            resultSet.close();
        } catch (SQLException e) {
            Log.error("An error occurred in the database handler");
            Minecraft.getInstance().player.sendMessage(new StringTextComponent("Error: SQLException"));
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return results;
    }

    public static Map<String, Object> getDataById(int id) {
        Connection connection;
        ResultSet resultSet = null;
        PreparedStatement statement = null;
        Map<String, Object> results = new HashMap<>();

        try {
            connection = getConnection();
        } catch (SQLException e) {
            Log.error(e.getMessage());
            Log.error("MYSQL_error");
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException e) {
            Log.error("Class for the given connection is not available, please use another one");
            return null;
        }

        try {
            statement = connection.prepareStatement("SELECT * FROM data WHERE id = ?");
            statement.setInt(1, id);
            Log.info(statement);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                    Object resultSetObject = resultSet.getObject(i);
                    if (resultSetObject == null){
                        results.put(resultSet.getMetaData().getColumnName(i), "null");
                    }
                    else {
                        results.put(resultSet.getMetaData().getColumnName(i), resultSetObject);
                    }

                }
            }
            resultSet.close();
        } catch (SQLException e) {
            Log.error("An error occurred in the database handler");
            Minecraft.getInstance().player.sendMessage(new StringTextComponent("Error: SQLException"));
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return results;
    }

    public static int getAmountOfBlocksAtX(int other_x, int other_z, int id, boolean isStart) {
        Connection connection;
        ResultSet resultSet = null;
        PreparedStatement statement = null;
        Map<String, Integer> results = new HashMap<>();
        int amount = 0;

        try {
            connection = getConnection();
        } catch (SQLException e) {
            Log.error(e.getMessage());
            Log.error("MYSQL_error");
            e.printStackTrace();
            return 0;
        } catch (ClassNotFoundException e) {
            Log.error("Class for the given connection is not available, please use another one");
            return 0;
        }
        try {
            if (isStart){
                statement = connection.prepareStatement("SELECT COUNT(id) AS amount FROM minecraft " +
                        "WHERE end_x = ? AND end_z = ? AND start_x is not null AND start_z is not null AND id <= ?");
            }
            else {
                statement = connection.prepareStatement("SELECT COUNT(id) AS amount FROM minecraft " +
                        "WHERE start_x = ? AND start_z = ? AND end_x is not null AND end_z is not null AND id <= ?");
            }
            statement.setInt(1, other_x);
            statement.setInt(2, other_z);
            statement.setInt(3, id);

            Log.info(statement);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                    amount = resultSet.getInt(i);
                    Log.info(resultSet.getMetaData().getColumnName(i), " = " + amount);

                }
            }
        } catch (SQLException e) {
            Log.error("An error occurred in the database handler");
            Minecraft.getInstance().player.sendMessage(new StringTextComponent("Error: SQLException"));
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return amount;
    }
}
