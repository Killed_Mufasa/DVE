package com.dve.dve3demo.commands;

import com.dve.dve3demo.network.Networking;
import com.dve.dve3demo.network.PacketOpenData;
import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.StringTextComponent;
import net.minecraftforge.fml.network.NetworkDirection;

public class CommandData implements Command<CommandSource> {

    private static final CommandData CMD = new CommandData();

    public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
        return Commands.literal("data")
                .requires(cs -> cs.hasPermissionLevel(0))
                .executes(CMD);
}

    @Override
    public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
        context.getSource().sendFeedback(new StringTextComponent("Data shown"), false);
        ServerPlayerEntity player = context.getSource().asPlayer();
        Networking.INSTANCE.sendTo(new PacketOpenData(), player.connection.netManager, NetworkDirection.PLAY_TO_CLIENT);
        return 0;
    }
}