package com.dve.dve3demo.commands;

import com.dve.dve3demo.gui.SpawnerScreen;
import com.dve.dve3demo.util.HeatmapSpawner;
import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import net.minecraft.client.Minecraft;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.util.text.StringTextComponent;

public class CommandLoad implements Command<CommandSource> {

    private static final CommandLoad CMD = new CommandLoad();

    public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
        return Commands.literal("load")
                .requires(cs -> cs.hasPermissionLevel(0))
                .executes(CMD);
    }

    @Override
    public int run(CommandContext<CommandSource> context) {
        SpawnerScreen.ySpawnHeight = SpawnerScreen.yMaxHeight;
        SpawnerScreen.rowsVisualised = SpawnerScreen.rowsVisualised + SpawnerScreen.rowBatchSize;
        context.getSource().sendFeedback(new StringTextComponent("Loading visualisation with" + SpawnerScreen.rowBatchSize + " rows, total becomes: " + SpawnerScreen.rowsVisualised), false);
        HeatmapSpawner.loadHeatmap(Minecraft.getInstance(), SpawnerScreen.blockStateStart, SpawnerScreen.blockStateEnd, SpawnerScreen.rowBatchSize, SpawnerScreen.stopAtXBatches, false, true);
        return 0;
    }
}