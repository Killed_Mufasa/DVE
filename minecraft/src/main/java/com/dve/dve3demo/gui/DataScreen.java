package com.dve.dve3demo.gui;

import com.dve.dve3demo.DVE;
import com.dve.dve3demo.items.DataInspectorItem;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;

import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;

public class DataScreen extends Screen {

    // Set default sizes in pixels
    private static final int WIDTH = 572;
    private static final int HEIGHT = 250;
    public static ArrayList<TextFieldWidget> dataKeyTxtFields = new ArrayList<>();
    public static ArrayList<TextFieldWidget> dataValueTxtFields = new ArrayList<>();

    // Alternative to database: keep a list of all blockPos
    // Initialize a new ArrayList to store blockPos' in (only for reading from an XML)
    // private ArrayList<BlockPos> blockPosList = new ArrayList<>();

    // Initialize a new ResourceLocation for the GUI with a specified background
    private final ResourceLocation GUI = new ResourceLocation(DVE.MODID, "textures/gui/spawner_gui_new3.png");

    // Made private! - Set a textComponent to explain what this GUI does
    private DataScreen() {
        super(new StringTextComponent("This is the data of the row belonging to the block you clicked"));
    }

    @Override
    protected void init() {

        // Set the FontRenderer to the one used by the ingame GUI
        FontRenderer fontRenderer = getMinecraft().ingameGUI.getFontRenderer();
        // Calculate the relative values; the middle of the GUI
        int relX = (this.width - WIDTH) / 2 + 10;
        int relXrow2 = relX + 92;
        int relY = (this.height - HEIGHT) / 2 + 10;

        int i = 0;
        for (Map.Entry<String, Object> entry : DataInspectorItem.dataFromDatabase.entrySet()) {
            Minecraft.getInstance().player.sendChatMessage(entry.getKey() + " = " + entry.getValue().toString());

            TextFieldWidget textFieldWidget = null;
            textFieldWidget = new TextFieldWidget(fontRenderer,
                    relX,
                    relY + (i*23),
                    86,
                    20,
                    "Data Key " +  i);
            textFieldWidget.setTextColor(22222222);
            textFieldWidget.setMaxStringLength(45);
            textFieldWidget.setText(entry.getKey());
            dataKeyTxtFields.add(textFieldWidget);
            addButton(textFieldWidget);

            TextFieldWidget textFieldWidget2 = null;
            textFieldWidget2 = new TextFieldWidget(fontRenderer,
                    relXrow2,
                    relY + (i*23),
                    86,
                    20,
                    "Data Key " +  i);
            textFieldWidget.setTextColor(55555555);
            textFieldWidget2.setMaxStringLength(45);
            textFieldWidget2.setText(entry.getValue().toString());
            dataValueTxtFields.add(textFieldWidget2);
            addButton(textFieldWidget2);

            i++;

            if (i == 10 || i == 20){
                relX = relX + 188;
                relXrow2 = relX + 92;
                relY = relY - 230;
            }
        }
    }


    // Pause the game (default false)
    @Override
    public boolean isPauseScreen() {
        return false;
    }

    // Render the GUI
    @Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        Objects.requireNonNull(this.minecraft).getTextureManager().bindTexture(GUI);
        int relX = (this.width - WIDTH) / 2;
        int relY = (this.height - HEIGHT) / 2;
        this.blit(relX, relY, 0, 0, WIDTH, HEIGHT);
        super.render(mouseX, mouseY, partialTicks);
    }

    // Open the GUI
    public static void open() {
        Minecraft.getInstance().displayGuiScreen(new DataScreen());
    }
}
